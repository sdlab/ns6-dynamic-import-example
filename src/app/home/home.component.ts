import { Component, OnInit } from "@angular/core";
import * as fs from 'tns-core-modules/file-system';
declare var __non_webpack_require__: any;

@Component({
    selector: "Home",
    moduleId: module.id,
    templateUrl: "./home.component.html"
})
export class HomeComponent implements OnInit {

    constructor() {
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {


        const file = fs.knownFolders.currentApp().getFile('basic-module-example.js');

        console.log('js content', file.path, file.readTextSync());

        const modules = __non_webpack_require__(file.path);

        console.log('modules', modules);

    }
}
