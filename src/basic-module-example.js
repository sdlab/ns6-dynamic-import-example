(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('nativescript-angular/router'), require('@angular/core')) :
    typeof define === 'function' && define.amd ? define('flex-app-ns', ['exports', 'nativescript-angular/router', '@angular/core'], factory) :
    (factory((global['flex-app-ns'] = {}),global.router,global.ng.core));
}(this, (function (exports,router,core) { 'use strict';

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var HelloComponent = /** @class */ (function () {
        function HelloComponent() {
        }
        HelloComponent.decorators = [
            { type: core.Component, args: [{
                        selector: 'app-hello',
                        template: "<label text=\"Hello!\"></label>"
                    }] }
        ];
        return HelloComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /** @type {?} */
    var appRoutes = [
        { path: '', redirectTo: 'loading-page', pathMatch: 'full' },
        { path: 'loading-page', component: HelloComponent }
    ];
    // Config.PLATFORM_TARGET = Config.PLATFORMS.MOBILE_NATIVE;
    var BaseModule = /** @class */ (function () {
        function BaseModule() {
            console.log('BaseModule constructor');
        }
        BaseModule.decorators = [
            { type: core.NgModule, args: [{
                        declarations: [HelloComponent],
                        imports: [
                            router.NativeScriptRouterModule,
                            router.NativeScriptRouterModule.forChild(appRoutes),
                        ],
                        schemas: [core.NO_ERRORS_SCHEMA]
                    },] }
        ];
        /** @nocollapse */
        BaseModule.ctorParameters = function () { return []; };
        return BaseModule;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */

    exports.BaseModule = BaseModule;
    exports.ɵa = HelloComponent;

    Object.defineProperty(exports, '__esModule', { value: true });

})));

//# sourceMappingURL=flex-app-ns.umd.js.map